---
sidebar: auto
---

# Les projets

## Les projets soutenus

### ODASS

[ODASS](https://www.odass.org) est une coopérative (SCIC) basée à Palaiseau.

![Logo d'ODASS](https://garecentrale.odass.org/files/PageHeader_ODASS_logo_noir_1000_vignette_97_97_20191216180144_20191216170236.png)

L'entreprise accompagne les structures et collectifs évoluant dans l’univers de l’économie sociale et solidaire à l’utilisation d’outils numériques collaboratifs et de gestion, de préférence libres et éco-responsables. Ces outils stables, pérennes et simples d'usage, sont adaptés et choisis avec et pour ses clients, afin de mieux servir leurs projets.

La cigales NOE (en gestion, trouve sa continuité dans la Cigales des trois rivières) a investi dans ODASS en 2017 et continue de suivre son développement.

## Les projets à l'étude

### Les Z’Echoppes

Entreprise (SAS) créée en 2019 basée à Versailles.

Les Z'Echoppes est une plateforme e-commerce de circuit court national où on trouve des ventes de produits alimentaires et artisanaux qui sont toutes associée à un village de France ; pour permettre aux artisans et aux producteurs d’être mieux rémunérés et de s’ancrer durablement dans leur territoire en leur donnant accès à un marché national tout en gardant les bénéfices du circuit court.

### BOSS

BOSS (Boutique des organisations sociales et solidaires) est un projet initié en 2019 à Montgeron.

Il s’agit de la création d’une boutique proposant produits locaux alimentaires et non alimentaires et services de l’économie sociale et solidaire à l’échelle départementale (aide au recrutement des entreprises de l’ESS). Cette boutique se veut être le lieu représentatif du secteur à la fois dans son activité mais aussi dans son éthique. Ainsi les notions de circuit court, écologie, innovation sociale, silver économie, solidarité, échange et insertion sont au cœur de son développement.

Le projet est actuellement à la recherche d’un local convenant à son activité, dans le nord-est Essonne (Montgeron, draveil, etc.).
