# Participez au club - devenez investisseur solidaire

La Cigales des trois rivières a été initiée en 2019 à la suite de la Cigales NOE. Elle souhaite développer le mouvement sur le département de l’Essonne. Rejoignez-nous !

## Un club CIGALES c'est quoi ?

Tout d'abord, une CIGALES c'est un acronyme pour **Club d'Investisseurs pour une Gestion Alternative et Locale de l'Epargne Solidaire**. Oui c'est long, donc on préfère dire CIGALES.

**Un club CIGALES est un collectif de 5 à 20 personnes** qui mettent dans une cagnotte commune une partie de leur propre épargne, entre 8 € et 450 € par mois et par personne (en moyenne 23 €). L'argent réuni permet à ce collectif d'investir directement (sans passer par un établissement financier) dans des entreprises, coopératives et associations de leur quartier, leur ville, leur département ou leur région.
Dans le respect de la charte des CIGALES, ces entreprises auront comme spécificité de mener des activités ayant une vocation sociale, solidaire, environnementale et/ou culturelle.

Des exemples ? Une chocolaterie équitable, une boulangerie bio d'insertion, une coopérative d'agriculture urbaine, un fournisseur d’électricité verte, une librairie, un théâtre, etc.

**Et puis ce n'est pas tout ...** Au delà du soutien financier apporté par le club CIGALES, celui-ci s'engage à suivre et accompagner l'entreprise afin de la faire bénéficier des compétences que détiennent ses membres. Ils développent donc une relation particulière avec le ou les porteurs de projet qu'ils accompagnent.
