---
heroImage: /logo_cleaned.svg
home: true
heroText: CIGALES des 3 rivières
tagline:
footer: CIGALES des 3 rivères - Club d'Investisseurs pour une Gestion Alternative et Locale de l'Epargne Solidaire
---

<span style="padding:20px"/>

# Une CIGALES, c’est quoi ?

Un club d'investisseurs de 5 à 20 citoyens qui soutient pendant 5 ans des initiatives locales ayant une plus-value sociale,
environnementale ou culturelle portées par des entreprises, des coopératives ou des associations.

<CustomButton display-text="Besoin d’aide pour entreprendre ?" link="/financez"/>

# Comment ça fonctionne ?

## Un soutien financier

Chaque membre épargne entre 8 et 450 € par mois.

La CIGALES investit l’épargne collectée en entrant au capital des entreprises ou en proposant un apport à taux zéro avec
droit de reprise aux associations.

Cet apport peut faire effet de levier pour d’autre financements : soutien d’autres CIGALES de l’Ile-de-France si le
projet présenté est retenu par celles-ci, Garrigue, Finansol, financement participatif, etc.


## Un accompagnement de proximité

Les membres peuvent également accompagner les porteurs : compétences, soutien moral, expérience, temps, réseau, etc.


<CustomButton display-text="Les projets que nous soutenons" link="/projets"/>
