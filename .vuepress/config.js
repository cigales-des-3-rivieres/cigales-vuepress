module.exports = {
    themeConfig: {
        logo: '/logo_cleaned_with_text.svg',
        search: false,
        nav: [
            {text: 'Financez votre projet', link: '/financez'},
            {text: 'Participez au club', link: '/participez'},
            {text: 'Les projets', link: '/projets'},
            {text: 'Contact', link: '/contact'}
        ]
    },
    dest: 'public',
    base: '/',
    head: [
        ['link', {rel: 'stylesheet', href: 'https://pro.fontawesome.com/releases/v5.10.0/css/all.css'}]
    ]
};
