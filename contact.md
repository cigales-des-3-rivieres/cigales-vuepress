# Contact

- <i class="fa fa-phone"/> 06 82 23 13 89 (Jean-Bernard Koechlin)
- <i class="fa fa-envelope"/> [cigalesdes3rivieres@orange.fr](mailto:cigalesdes3rivieres@orange.fr)

Club d'investisseur enregistré le 08/07/2019 sous le numéro 9104P61 2019 A 04620.

# Autres liens

- [L'association régionale des CIGALES d'Ile-de-France](http://www.cigales-idf.asso.fr/)
- [La Fédération Nationale des CIGALES](http://www.cigales.asso.fr)


