---
sidebar: auto
---

# Financez votre projet

::: tip Vous :
- portez un projet à **plus-value sociale, environnementale ou culturelle** ? 
- **recherchez des financements** pour votre projet mais pas que ? 
- êtes **sensible à la finance solidaire** ?
:::


##  Les structures éligibles

**Les sociétés à capitaux collectifs** : SARL, SA, SAS, etc. Les CIGALES interviennent au capital. Elles peuvent, selon vos besoins, faire un complément de financement en compte courant d'associé.

**Les coopératives** : SCOP, SCIC, Loi 47, etc. Là encore, les CIGALES interviennent au capital et deviennent sociétaires de votre coopérative.

**Les associations** : Apport avec droit de reprise dont les modalités (montant et échéance) de remboursement sont définies directement avec vous.


##  Les critères de financement

### Les conditions

Pour obtenir un financement de la part des CIGALES, 3 conditions :
- Le siège social ou l'activité de votre structure doit se situer en Île-de-France
- L'activité de votre structure doit impérativement avoir un impact social, environnemental ou culturel
- Le statut juridique de votre structure doit correspondre aux outils de financement des CIGALES (voir plus haut)

### Les critères favorables

Les clubs CIGALES, acteurs de la finance solidaire, sont sensibles au statut et au parcours du porteur de projet :

- Entrepreneuriat féminin
- Personne éloignée de l'emploi (jeune, bénéficiaire des minima sociaux, en situation de handicap, etc.)
- Porteurs issus de ou s'installant dans un Quartier Politique de la Ville

Votre projet correspond à ces différents critères ? Vous êtes convaincu par la démarche et souhaitez être financé et accompagné par une ou plusieurs CIGALES ? 
